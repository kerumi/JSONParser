using System;

namespace JSONParser
{
    public class SyntaxException : Exception
    {
        public SyntaxException(string message) : base(message){
            
        }
    }
}